<?php

class pdf_viewsController extends adminController {
	
	function __construct(){
		parent::__construct("Pdf_View");
	}
	
	function index(Array $params = []){

		// $this->_viewData->hasCreateBtn = true;

		$params['queryOptions'] = ['page_size' => 25];

		global $emagid;
		$db = $emagid->getDb();

		$sql = "select pdf_no, count(*) as view_counts from pdf_view_counts where view_type='phone' and active = 1 group by pdf_no order by pdf_no;";	
		$results = $db->getResults($sql);
		$this->_viewData->pdf_view_counts = $results;
		parent::index($params);
	}
}