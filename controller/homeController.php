<?php

use Twilio\Rest\Client;

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){
        $this->loadView($this->viewData);
    }

    function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function submit_survey(){
        $resp['status'] = false;
        if(isset($_POST) && $_POST != ''){
            $survey_answer = \Model\Survey::loadFromPost();
            if($survey_answer->save()){
                $resp['status'] = true;
            }
        }
        $this->toJson($resp);
    }

    function quiz(Array $params = [])
    {
        $questions = \Model\Question::getList(['orderBy'=>'display_order ASC']);
        foreach ($questions as $i=> $question){
            $questions[$i]->answers = $question->get_answers();
        }
        $this->viewData->questions = $questions;

        $this->loadView($this->viewData);
    }


    public function pdfs_post(){
        $resp['status'] = false;
        if(isset($_POST['phone']) && isset($_POST['link']) && $_POST['phone'] != '' && $_POST['link'] != ''){
            $phone = $_POST['phone'];
            $link = $_POST['link'];
            $pdf_no = $_POST['pdf_no'];
            $sid = TWILIO_SID;
            $token = TWILIO_TOKEN;
            $client = new Client($sid, $token);

            $pdf_views_count = new \Model\Pdf_View();
            $pdf_views_count->phone = $phone;
            $pdf_views_count->pdf_no = $pdf_no;
            $pdf_views_count->view_type = 'phone';
            $pdf_views_count->save();

            try {
                $client->messages
                ->create(
                    $phone,
                    array(
                        "from" => TWILIO_NUMBER,
                        "body" => "Here's you PDF link - ".$link
                    )
                );
                $resp['status'] = true;
                $resp['msg'] = "success";
            } catch(Twilio\Exceptions\TwilioException $e) {
                $resp['status'] = false;
                $resp['msg'] = "Failed sending link";
            }
        }
        $this->toJson($resp);
    }


    public function survey_post(){
        $resp['status'] = false;
        if(isset($_POST['phone']) && isset($_POST['store']) && $_POST['phone'] != '' && $_POST['store'] != ''){
            $phone = $_POST['phone'];
            $store = $_POST['store'];
            $link = '';
            if($store == 'apple'){
                $link = "https://itunes.apple.com/us/app/cognizant-perspectives/id439247923?mt=8";
            } else if($store == 'google'){
                $link = "https://play.google.com/store/apps/details?id=com.cognizanttechnologysolutions.perspectivesapp&hl=en";
            }
            $sid = TWILIO_SID;
            $token = TWILIO_TOKEN;
            $client = new Client($sid, $token);

            $app_downloads_count = new \Model\App_Download();
            $app_downloads_count->phone = $phone;
            $app_downloads_count->app_store = $store;
            $app_downloads_count->download_through = 'text';
            $app_downloads_count->save();

            try {
                $client->messages
                ->create(
                    $phone,
                    array(
                        "from" => TWILIO_NUMBER,
                        "body" => "Here's the App Store download link - ".$link
                    )
                );
                $resp['status'] = true;
                $resp['msg'] = "success";
            } catch(Twilio\Exceptions\TwilioException $e) {
                $resp['status'] = false;
                $resp['msg'] = "Failed sending link";
            }   
        }
        $this->toJson($resp);
    }

    public function submit_quiz() {
        $resp['status'] = false;
        if(isset($_POST['email']) && $_POST['email'] != ''){
            $article_rating = new \Model\Article_Rating($_POST);
            if($article_rating->save()){
                $resp['status'] = true;
            } 
        }
        $this->toJson($resp);
    }
   
}