<?php

namespace Model;

class Article_Rating extends \Emagid\Core\Model {
    static $tablename = "public.article_ratings";

    public static $fields  =  [
        'email',
        'article',
        'rating',
    ];
}