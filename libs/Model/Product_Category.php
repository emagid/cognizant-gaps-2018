<?php

namespace Model;

class Product_Category extends \Emagid\Core\Model {
  
  static $tablename = "product_categories";
  
  public static $fields = [
    'product_map_id',
    'category_id'
  ];
  
  static $relationships = [
  	[
  		'name'=>'product_map',
  		'class_name' => '\Model\Product_Map',
  		'local'=>'product_map_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  	[
  		'name'=>'category',
  		'class_name' => '\Model\Category',
  		'local'=>'category_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  ];

  public static function get_product_categories($product_id, $limit = "") {
    $pc = self::getList(['where'=>"product_id={$product_id} AND active=1", 'limit'=>$limit]);
    $arr = [];
    foreach($pc as $c) {
      $arr[] = $c->category_id;
    }
    return $arr;
  }

  public static function get_category_products($category_id, $limit = "", $except = "") {
  	$where = "category_id={$category_id} AND active=1 ";
  	if ($except != ""){
  		$where .= $except;
  	}
    $pc = self::getList(['where'=>$where, 'limit'=>$limit]);
    $list = [];
    foreach($pc as $c) {
      if(\Model\Product_Map::getItem($c->product_map_id)){
        $list[] = \Model\Product_Map::getItem($c->product_map_id)->product();
      }
    }
    return $list;
  }

  public function getProductCategories($id, $type){
    $map = new Product_Map();
    $category = self::getList(['where'=>"product_map_id = ".$map->getMap($id, $type)->id]);
    $list = [];
    foreach($category as $cat){
      $list[] = $cat->category_id;
    }
    return $list;
  }

  public function getCategoryProducts($categoryId){
    $category = self::getList(['where'=> "category_id = $categoryId and active = 1"]);
    $list = [];
    foreach($category as $item){
      $productMap = Product_Map::getItem($item->product_map_id);
      $map = new Product_Map();
      $list[] = $map->getProduct($productMap->id, $productMap->product_type_id);
    }
    return $list;
  }
}
