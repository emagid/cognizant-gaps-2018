<?php

namespace Model;

class Vendor extends \Emagid\Core\Model {
    static $tablename = "public.vendor";

    public static $fields  =  [
    	'authorized'=>['type'=>'boolean'],
    	'email'=>['required'=>true,'type'=>'email','unique'=>true],
    	'password'=>['required'=>true],
    	'hash',
    	'first_name'=>['required'=>true,'name'=>'First Name'],
    	'last_name'=>['required'=>true,'name'=>'Last Name'],
    	 'zip'=>['required'=>true,'type'=>'numeric'],
    	'phone'=>['required'=>true,'type'=>'numeric'], 
    	 
    	'key',
    	'step_2_completed'=>['type'=>'boolean'],
    	
    	//'dob'=>['required'=>true, 'name'=>'Date of Birth'],
    	'experience',
    	
    	'description',
    	'teaching_description',
    	'promise_statement',
    	'certificates',
    	'lifestyle',

    	'testimonial_1',
    	'testimonial_1_reference',
    	'testimonial_2',
    	'testimonial_2_reference',
    	'testimonial_3',
    	'testimonial_3_reference',

    	'photo',
    	'license_photo',
    	'extra_photo_1',
    	'extra_photo_2',

    	'agreed_manifesto',
		'agreed_ethics',
		'agreed_terms',
		'agreed_privacy',
        'title',
        'description_1',
        'description_2',
        'description_3' 
    ];

    public function updateSteps(){
    	$fields = [
    		'email',
	    	'password',
	    	'first_name',
	    	'last_name',
	    	'zip',
	    	'phone', 
	    	'dob',
	    	'experience',
	    	'description',
	    	'teaching_description',
	    	'promise_statement',
	    	'certificates',
	    	'lifestyle',
	    	'testimonial_1',
	    	'testimonial_1_reference',
	    	'testimonial_2',
	    	'testimonial_2_reference',
	    	'testimonial_3',
	    	'testimonial_3_reference',
	    	'photo',
	    	'license_photo',
	    	'extra_photo_1',
	    	'extra_photo_2',
	    	'agreed_manifesto',
			'agreed_ethics',
			'agreed_terms',
			'agreed_privacy',
            'title',
            'description_1',
            'description_2',
            'description_3' 
    	];

    	$valid = true;
    	foreach ($fields as $field){
    		if (is_null($this->$field) || trim($this->$field) == ""){
    			$valid = false;
    		}
    	}

    	$packages = \Model\Package::getList(['where'=>'vendor_id = '.$this->id]);

    	if ($valid && count($packages > 0)){
    		$this->step_2_completed = true;
    		$this->save();
    	}
    }

    static $relationships = [
      [
        'name'=>'vendor_service',
        'class_name' => '\Model\Vendor_Services',
        'local'=>'id',
        'remote'=>'vendor_id',
        'remote_related'=>'service_id',
        'relationship_type' => 'many'
      ]
    ];

    /**
    * concatenates first name and last name to create full name string and returns it
    * @return type: string of full name
    */
    function full_name() {
        return $this->first_name.' '.$this->last_name;
    }

    /**
    * Verify login and create the authentication cookie / session 
    * Accepts no parameter to login the instantiated object or 2 parameters (login and password) 
    * to get an object from the database and log it in
    */

    public function login(){
    	if (count(func_get_args()) == 0){
	    	$roles = \Model\Vendor_Roles::getList(['where' => 'active = 1 and vendor_id = '.$this->id]);
	    	if (!is_null($roles)){
	    		$rolesIds = [];
		        foreach($roles as $role){
		            $rolesIds[] = $role->role_id;
		        }
		        $rolesIds = implode(',', $rolesIds);

		        $roles = \Model\Role::getList(['where' => 'active = 1 and id in ('.$rolesIds.')']);
		        $rolesNames = [];
		        foreach($roles as $role){
		            $rolesNames[] = $role->name;
		        }

		        \Emagid\Core\Membership::setAuthenticationSession($this->id, $rolesNames, $this);

		        return true;
	    	} else {
	    		return false;
	    	}
    	} else if (count(func_get_args()) == 2){
    		$email = func_get_arg(0);
    		$password = func_get_arg(1);
			$user = self::getItem(null, ['where'=>"email = '".$email."'"]);
	        if (!is_null($user)){
	            $hash = \Emagid\Core\Membership::hash($password, $user->hash);            
	            if ($hash['password'] == $user->password) {
	                $user->login();
	                return true;
	            } else {
	            	return false;
	            }
	        } else {
	        	return false;
	        }
    	} else {
    		return false;
    	}
    }

}
























