<div class="modal_overlay_wrapper" id="modal_overlay_register">
	<div class="hide_register_overlay hide_modal_overlay">
		<svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
            <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
        </svg>
		<p>Close</p>
	</div>
	<div class = "centered_wrapper signin_box register_box">
		<a href="<?=SITE_URL?>"><img class="login_logo" src="<?=FRONT_ASSETS?>img/logo_new.png"></a>
		<h4>Create account</h4>
		<form class = "signin_form" method="post" action="/wholesale" id="wholesale-form">
            <input type="hidden" name="id" value="0"/>
			<div class="a_row">
				<label for="first_name">First Name</label>
				<input id="first_name" type="text" name="first_name" class="login_page_input" placeholder="First Name" />
			</div>
            <div class="a_row">
                <label for="last_name">Last name</label>
                <input id="last_name" type="text" name="last_name" class="login_page_input" placeholder="Last Name" />
            </div>
            <div class="a_row">
                <label for="company">Company</label>
                <input id="company" type="text" name="company" class="login_page_input" placeholder="Company Name" />
            </div>
            <div class="a_row">
                <label for="phone">Phone</label>
                <input id="phone" type="tel" name="phone" class="login_page_input" placeholder="Phone Number" />
            </div>
			<div class="a_row">
                <label for="email">Email address</label>
                <input id="email" type="email" name="email" class="login_page_input" placeholder="Email" />
            </div>
            <div class="a_row">
                <label for="full_address">Full Address</label>
                <input id="full_address" type="text" name="full_address" class="login_page_input" placeholder="Full Address" />
            </div>
			<div class = "a_row register_sugmit_row">
				<input type="submit" class="register_page_submit" value="Submit"/>
			</div>
		</form>
		<div class="a_shadow_divider">
			
		</div>
		<h5 class="register_final_txt">Already have an account?<a class="show_signin_overlay">Sign in</a></h5>

	</div>
</div>