<?php
 if(count($model->app_downloads)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
            <th width="15%">App Store</th>
            <th width="15%">Total Downloads</th>	
        </tr>
      </thead>
      <tbody>
       <!-- <?php //foreach($model->contacts as $obj){ ?> -->
        <tr>
            <td><a href="">Apple</a></td>
            <td><a href=""><?php echo $model->apple_downloads_count; ?></a></td>
        </tr>

        <tr>
            <td><a href="">Google</a></td>
            <td><a href=""><?php echo $model->google_downloads_count; ?></a></td>
        </tr>
       <!-- <?php //} ?> -->
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'app_downloads';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>