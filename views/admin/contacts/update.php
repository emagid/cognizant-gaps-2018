<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
 
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="test">
        <input type="hidden" name="id" value="<?php echo $model->contact->id; ?>" />
        <input name="token" type="hidden" value="<?php echo get_token();?>" />
        <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Name</label>
                        <?php echo $model->form->editorFor("name"); ?>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <?php echo $model->form->editorFor("email"); ?>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <?php echo $model->form->editorFor("phone"); ?>
                    </div>
                    <div class="form-group">
                        <label>Form Used</label>
                        <input type="text" disabled value="<?=\Model\Contact::$forms[$model->contact->form]?>">
                    </div>
                    <div class="form-group">
                        <label>Donation Amount</label>
                        <?php echo $model->form->textboxFor("donation",['disabled'=>true]); ?>
                    </div>
                </div>
            </div>
 
        </div>
      </div>
    
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
 