<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
	  	<li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab" data-toggle="tab">Account Information</a></li>
	  	<li role="presentation"><a href="#profile-info-tab" aria-controls="profile-info" role="tab" data-toggle="tab">Profile Information</a></li>
	  	<li role="presentation"><a href="#description-tab" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
	  	<li role="presentation"><a href="#packages-tab" aria-controls="packages" role="tab" data-toggle="tab">Packages</a></li>
	</ul>
	<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
		<input type="hidden" name="id" value="<?php echo $model->provider->id;?>" />
		<input type="hidden" name="step_2_completed" value="<?php echo $model->provider->step_2_completed;?>" />
		<div class="tab-content">
      		<div role="tabpanel" class="tab-pane active" id="account-info-tab">
          		<div class="row">
              		<div class="col-md-12">
                  		<div class="box">
							<h4>Account Information</h4>
							<div class="form-group">
								<?php echo $model->form->checkBoxFor('authorized', 1);?>
								<label>Authorized</label>
							</div>
							<div class="form-group">
								<label>Email Address</label>
								<?php echo $model->form->editorFor('email');?>
							</div>
							<div class="form-group">
								<label>First Name</label>
								<?php echo $model->form->editorFor('first_name');?>
							</div>
							<div class="form-group">
								<label>Last Name</label>
								<?php echo $model->form->editorFor('last_name');?>
							</div>
							<div class="form-group">
								<label>Password (leave blank to keep current password)</label>
								<input type="password" name="password" />
							</div>
                  		</div>
              		</div>
              		<div class="col-md-12">
                  		<div class="box">
                  			<h4>Contact Details</h4>
                  			<div class="form-group">
								<label>Zip Code</label>
								<?php echo $model->form->editorFor('zip');?>
							</div>
							<div class="form-group">
								<label>Phone Number</label>
								<?php echo $model->form->editorFor('phone');?>
							</div>
                  		</div>
                  	</div>
          		</div>
        	</div>
        	<div role="tabpanel" class="tab-pane" id="profile-info-tab">
        		<div class="row">
        			<div class="col-md-12">
						<div class="box">
                  			<h4>Profile Information</h4>
                  			<div class="form-group">
								<label>Profile Photo</label>
								<p><small>(ideal profile photo size is 850 x 850)</small></p>
								<p><input type="file" name="photo" class='image' /></p>
								<div style="display:inline-block">
									<?php 
										$img_path = "";
										if($model->provider->photo != "" && file_exists(UPLOAD_PATH.'providers'.DS.$model->provider->photo)){ 
											$img_path = UPLOAD_URL . 'providers/' . $model->provider->photo;
									?>
											<div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
												<img src="<?php echo $img_path; ?>" />
												<br />
												<a href="<?= ADMIN_URL.'providers/delete_image/'.$model->provider->id;?>?photo=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
												<input type="hidden" name="photo" value="<?=$model->provider->photo?>" />
											</div>
									<?php } ?>
									<div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
								</div>
							</div>
							<div class="form-group">
								<label>Birthday</label>
								<div class="row">
									<div class="col-md-10">
										<label>Month</label>
										<?php echo $model->form->dropDownListFor('mob', get_month(), '', ['class'=>'form-control']);?>
									</div>
									<div class="col-md-7">
										<label>Day</label>
										<?php echo $model->form->dropDownListFor('dob', range(1, 31), '', ['class'=>'form-control']);?>
									</div>
									<div class="col-md-7">
										<label>Year</label>
										<?php echo $model->form->dropDownListFor('yob', range(date('Y')-20, date('Y')-100), '', ['class'=>'form-control']);?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Experience</label>
								<?  
									$experience = [
										1 => '1 year',
										2 => '2 years',
										3 => '3 years',
										4 => '4 to 5 years',
										6 => '6 to 7 years',
										8 => '8 to 9 years',
										10 => '10 or more years',
									];
								?>
								<?php echo $model->form->dropDownListFor('experience', $experience, '', ['class'=>'form-control']);?>
							</div>
                  		</div>
                  		<div class="box">
                  			<h4>Services</h4>
                  			<select name="provider_service[]" class="multiselect" data-placeholder="Services" multiple="multiple">
								<? foreach($model->services as $service) { ?>
									<option value="<?=$service->id?>"><?=$service->name?></option>
								<? } ?>
							</select>
                  		</div>
                  	</div>
					<div class="col-md-12">
                  		<div class="box">
                  			<h4>Testimonials</h4>
                  			<div class="form-group">
								<?php echo $model->form->textareaFor('testimonial_1');?>
							</div>
							<div class="form-group">
								<label>Reference</label>
								<?php echo $model->form->editorFor('testimonial_1_reference');?>
							</div>
						</div>
						<div class="box">
							<div class="form-group">
								<?php echo $model->form->textareaFor('testimonial_2');?>
							</div>
							<div class="form-group">
								<label>Reference</label>
								<?php echo $model->form->editorFor('testimonial_2_reference');?>
							</div>
						</div>
						<div class="box">
							<div class="form-group">
								<?php echo $model->form->textareaFor('testimonial_3');?>
							</div>
							<div class="form-group">
								<label>Reference</label>
								<?php echo $model->form->editorFor('testimonial_3_reference');?>
							</div>
                  		</div>
                  	</div>
        		</div>
        	</div>
        	<div role="tabpanel" class="tab-pane" id="description-tab">
        		<div class="row">
	        		<div class="col-md-8">
						<div class="box">
							<div class="form-group">
								<label>Bio Description</label>
								<?php echo $model->form->textareaFor('description');?>
							</div>
							<div class="form-group">
								<label>Teaching Style</label>
								<?php echo $model->form->textareaFor('teaching_description');?>
							</div>
							<div class="form-group">
								<label>Promise Statement</label>
								<?php echo $model->form->textareaFor('promise_statement');?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="box">
							<div class="form-group">
								<label>Lifestyle</label>
								<?php echo $model->form->textareaFor('lifestyle');?>
							</div>
							<div class="form-group">
								<label>Extra Photo 1</label>
								<p><input type="file" name="extra_photo_1" class='image' /></p>
								<div style="display:inline-block">
									<?php 
										$img_path = "";
										if($model->provider->extra_photo_1 != "" && file_exists(UPLOAD_PATH.'providers'.DS.$model->provider->extra_photo_1)){ 
											$img_path = UPLOAD_URL . 'providers/' . $model->provider->extra_photo_1;
									?>
											<div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
												<img src="<?php echo $img_path; ?>" />
												<br />
												<a href="<?= ADMIN_URL.'providers/delete_image/'.$model->provider->id;?>?type=extra_photo_1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
												<input type="hidden" name="extra_photo_1" value="<?=$model->provider->extra_photo_1?>" />
											</div>
									<?php } ?>
									<div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
								</div>
							</div>
							<div class="form-group">
								<label>Extra Photo 2</label>
								<p><input type="file" name="extra_photo_2" class='image' /></p>
								<div style="display:inline-block">
									<?php 
										$img_path = "";
										if($model->provider->extra_photo_2 != "" && file_exists(UPLOAD_PATH.'providers'.DS.$model->provider->extra_photo_2)){ 
											$img_path = UPLOAD_URL . 'providers/' . $model->provider->extra_photo_2;
									?>
											<div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
												<img src="<?php echo $img_path; ?>"/>
												<br />
												<a href="<?= ADMIN_URL.'providers/delete_image/'.$model->provider->id;?>?type=extra_photo_2" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
												<input type="hidden" name="extra_photo_2" value="<?=$model->provider->extra_photo_2?>" />
											</div>
									<?php } ?>
									<div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="box">
							<div class="form-group">
								<label>Certificates</label>
								<?php echo $model->form->textareaFor('certificates');?>
							</div>
							<div class="form-group">
								<label>License Photo</label>
								<p><input type="file" name="license_photo" class='image' /></p>
								<div style="display:inline-block">
									<?php 
										$img_path = "";
										if($model->provider->license_photo != "" && file_exists(UPLOAD_PATH.'providers'.DS.$model->provider->license_photo)){ 
											$img_path = UPLOAD_URL . 'providers/' . $model->provider->license_photo;
									?>
											<div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
												<img src="<?php echo $img_path; ?>" />
												<br />
												<a href="<?= ADMIN_URL.'providers/delete_image/'.$model->provider->id;?>?type=license_photo" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
												<input type="hidden" name="license_photo" value="<?=$model->provider->license_photo?>" />
											</div>
									<?php } ?>
									<div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
        	</div>
        	<div role="tabpanel" class="tab-pane" id="packages-tab">
        		<div class="row">
	        		<div class="col-md-24">
						<div class="box">
							<h4>Packages</h4>
						</div>
					</div>
				</div>
				<?php if(count($model->packages)>0): ?>
					<div class="row">
						<div class="col-md-24">
						    <div class="box box-table">
						        <table id="data-list" class="table">
						            <thead>
						                <tr>
						                	<th width="10%">Image</th>
						                  	<th width="60%">Title</th>
						                  	<th width="15%" class="text-center">Edit</th>	
						                  	<th width="15%" class="text-center">Delete</th>	
						                </tr>
						            </thead>
						            <tbody>
										<?php foreach($model->packages as $package){ ?>
											<tr class="originalProducts">
												<td>
													<?php 
														$img_path = ADMIN_IMG.'no-image.png';
														if(!is_null($package->image) && $package->image != "" && file_exists(UPLOAD_PATH.'packages'.DS.$package->image)){ 
															$img_path = UPLOAD_URL . 'packages/' . $package->image;
														}
													?>
													<a href="<?php echo ADMIN_URL; ?>packages/update/<?php echo $package->id; ?>"><img src="<?php echo $img_path; ?>" width="50" /></a>
												</td>
												<td><a href="<?php echo ADMIN_URL; ?>packages/update/<?php echo $package->id; ?>"><?php echo $package->title; ?></a></td>
												<td class="text-center">
													<a class="btn-actions" href="<?php echo ADMIN_URL; ?>packages/update/<?php echo $package->id; ?>">
														<i class="icon-pencil"></i> 
													</a>
												</td>
												<td class="text-center">
													<a class="btn-actions" href="<?php echo ADMIN_URL; ?>packages/delete/<?php echo $package->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
														<i class="icon-cancel-circled"></i> 
													</a>
												</td>
											</tr>
										<?php } ?>
						            </tbody>
						        </table>
						    </div>
						</div>
					</div>
				<?php endif; ?>
			</div>
        </div>
        <div class="form-group">
			<button type="submit" class="btn btn-save">Save</button>
		</div>
     </form>
</div>

<?php footer();?>

<script>

var services = <?php echo json_encode($model->provider->services); ?>;

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		var img = $("<img />");
		reader.onload = function (e) {
			img.attr('src',e.target.result);
			img.attr('alt','Uploaded Image');
			img.attr("width",'100%');
			img.attr('height','100%');
		};
		$(input).parent().parent().find('.preview-container').html(img);
		$(input).parent().parent().find('input[type="hidden"]').remove();

		reader.readAsDataURL(input.files[0]);
	}
}

$(function(){
	$("select[name='provider_service[]']").val(services);

	$("select.multiselect").each(function(i,e) {
        //$(e).val('');
        var placeholder = $(e).data('placeholder');
        $(e).multiselect({
            nonSelectedText:placeholder,
            includeSelectAllOption: true,
            maxHeight: 415,
            checkboxName: '',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%'
        });
    });

	$("input.image").change(function(){
		readURL(this);
	});
})

</script>