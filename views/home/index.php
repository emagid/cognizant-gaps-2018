<section class='homepage' style='background-image: url(<?= FRONT_ASSETS ?>img/background.jpg);'>
	<div class='content homepage_content'>
		<div class='welcome'>
			<p class='small'>Cognizant welcomes you to</p>
			<h1>GAPS 2019</h1>
		</div>

		<div class='message'>
			<p>Click below to get started!</p>
		</div>
		<div class='menu'>
			<div>
				<button id='pdfs' class='link' href="">THOUGHT LEADERSHIP <img src="<?= FRONT_ASSETS ?>img/btn_arr.png"></button>
				<button id='quiz' class='link' href="">PLAY THE TL RATING GAME <span>Win a Gift Card</span><img src="<?= FRONT_ASSETS ?>img/btn_arr.png"></button>
				<button id='survey' class='link' href="">LEARN MORE <img src="<?= FRONT_ASSETS ?>img/btn_arr.png"></button>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$('.white').fadeOut(1000);
</script>