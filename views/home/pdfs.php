
<div class='content'>    
    <div class='holder'>
        <i class="fa fa-close top"></i>
        <i class="fa fa-close bottom"></i>
        <div class='choices'>
            <p>Click the thumbnail of the asset you wish to download</p>
            <div class='pdf_slider'>
                <div>
                    <img class="pdf" data-pdf_no="pdf1" data-link="https://www.cognizant.com/whitepapers/extending-the-case-for-digital-health-plan-members-speak-codex3814.pdf" src="<?= FRONT_ASSETS ?>img/pdf1_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf1.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf1.pdf'>
                    <img class="pdf" data-pdf_no="pdf2" data-link="https://www.cognizant.com/whitepapers/wealth-management-how-digital-and-learning-algorithms-advance-holistic-advice-codex3825.pdf" src="<?= FRONT_ASSETS ?>img/pdf2_cover.jpg" data-ng='<?= FRONT_ASSETS ?>img/pdf2.pdf' data-pdf='<?= FRONT_ASSETS ?>img/pdf2.pdf' data-id='<?= FRONT_ASSETS ?>img/pdf2.png'>
                    <img class="pdf" data-pdf_no="pdf3" data-link="https://www.cognizant.com/whitepapers/rpa-is-just-the-start-how-insurers-can-develop-a-successful-intelligent-process-automation-strategy-codex3841.pdf" src="<?= FRONT_ASSETS ?>img/pdf3_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf3.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf3.pdf'>
                    <img class="pdf" data-pdf_no="pdf4" data-link="https://www.cognizant.com/whitepapers/financial-services-automation-taking-off-the-training-wheels-codex2873.pdf" src="<?= FRONT_ASSETS ?>img/pdf4_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf4.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf4.pdf'>
                    <img class="pdf" data-pdf_no="pdf5" data-link="https://www.cognizant.com/whitepapers/21-more-jobs-of-the-future-a-guide-to-getting-and-staying-employed-through-2029-codex3928.pdf" src="<?= FRONT_ASSETS ?>img/pdf5_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf5.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf5.pdf'>
                    <img class="pdf" data-pdf_no="pdf6" data-link="https://www.cognizant.com/whitepapers/making-ai-responsible-and-effective-codex3974.pdf" src="<?= FRONT_ASSETS ?>img/pdf6_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf6.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf6.pdf'>
                    <img class="pdf" data-pdf_no="pdf7" data-link="https://www.cognizant.com/whitepapers/enterprise-fusion-your-pathway-to-a-better-customer-experience-codex3845.pdf" src="<?= FRONT_ASSETS ?>img/pdf7_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf7.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf7.pdf'>
                    <img class="pdf" data-pdf_no="pdf8" data-link="https://www.cognizant.com/whitepapers/talent-intelligence-unlocking-people-data-to-redefine-how-humans-need-to-work-codex3895.pdf" src="<?= FRONT_ASSETS ?>img/pdf8_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf8.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf8.pdf'>
                    <img class="pdf" data-pdf_no="pdf9" data-link="https://www.cognizant.com/whitepapers/the-work-ahead-the-second-half-of-the-chessboard-media-and-entertainment-is-nearing-the-end-of-digitals-beginning-codex3527.pdf" src="<?= FRONT_ASSETS ?>img/pdf9_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf9.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf9.pdf'>
                </div>
                <div>
                    <img class="pdf" data-pdf_no="pdf10" data-link="https://www.cognizant.com/whitepapers/machine-learning-the-first-salvo-of-the-ai-business-revolution-codex3697.pdf" src="<?= FRONT_ASSETS ?>img/pdf10_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf10.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf10.pdf'>
                    <img class="pdf" data-pdf_no="pdf11" data-link="https://www.cognizant.com/whitepapers/the-road-to-ai-codex3614.pdf" src="<?= FRONT_ASSETS ?>img/pdf11_cover.jpg" data-ng='<?= FRONT_ASSETS ?>img/pdf11.pdf' data-pdf='<?= FRONT_ASSETS ?>img/pdf11.pdf' data-id='<?= FRONT_ASSETS ?>img/pdf11.png'>
                    <img class="pdf" data-pdf_no="pdf12" data-link="https://www.cognizant.com/whitepapers/the-culture-cure-for-digital-how-to-fix-whats-ailing-business-codex3673.pdf" src="<?= FRONT_ASSETS ?>img/pdf12_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf12.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf12.pdf'>
                    <img class="pdf" data-pdf_no="pdf13" data-link="https://www.cognizant.com/whitepapers/ai-ready-for-business-codex3752.pdf" src="<?= FRONT_ASSETS ?>img/pdf13_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf13.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf13.pdf'>
                    <img class="pdf" data-pdf_no="pdf14" data-link="https://www.cognizant.com/whitepapers/retails-next-frontier-codex3555.pdf" src="<?= FRONT_ASSETS ?>img/pdf14_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf14.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf14.pdf'>
                    <img class="pdf" data-pdf_no="pdf15" data-link="https://www.cognizant.com/whitepapers/every-move-you-make-privacy-in-the-age-of-the-algorithm-codex3684.pdf" src="<?= FRONT_ASSETS ?>img/pdf15_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf15.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf15.pdf'>
                    <img class="pdf" data-pdf_no="pdf16" data-link="https://www.cognizant.com/whitepapers/relearning-how-we-learn-from-the-campus-to-the-workplace-codex3921.pdf" src="<?= FRONT_ASSETS ?>img/pdf16_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf16.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf16.pdf'>
                    <img class="pdf" data-pdf_no="pdf17" data-link="https://www.cognizant.com/whitepapers/sales-marketing-and-service-optimization-strategies-for-accelerating-growth-codex3901.pdf" src="<?= FRONT_ASSETS ?>img/pdf17_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf17.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf17.pdf'>
                    <img class="pdf" data-pdf_no="pdf18" data-link="https://www.cognizant.com/whitepapers/digital-engineering-top-5-imperatives-for-communications-media-and-technology-companies-codex3771.pdf" src="<?= FRONT_ASSETS ?>img/pdf18_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf18.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf18.pdf'>
                </div>
                <div>
                    <img class="pdf" data-pdf_no="pdf19" data-link="https://www.cognizant.com/whitepapers/is-your-organization-ready-to-embrace-a-digital-twin-codex3636.pdf" src="<?= FRONT_ASSETS ?>img/pdf19_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf19.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf19.pdf'>
                    <img class="pdf" data-pdf_no="pdf20" data-link="https://www.cognizant.com/whitepapers/securing-the-digital-future-codex3141.pdf" src="<?= FRONT_ASSETS ?>img/pdf20_cover.jpg" data-ng='<?= FRONT_ASSETS ?>img/pdf20.pdf' data-pdf='<?= FRONT_ASSETS ?>img/pdf20.pdf' data-id='<?= FRONT_ASSETS ?>img/pdf20.png'>
                    <img class="pdf" data-pdf_no="pdf21" data-link="https://www.cognizant.com/whitepapers/how-life-and-annuity-companies-can-embrace-modern-platforms-to-boost-direct-to-consumer-capabilities-codex3842.pdf" src="<?= FRONT_ASSETS ?>img/pdf21_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf21.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf21.pdf'>
                    <img class="pdf" data-pdf_no="pdf22" data-link="https://www.cognizant.com/whitepapers/how-integrated-process-management-completes-the-blockchain-jigsaw-codex3435.pdf" src="<?= FRONT_ASSETS ?>img/pdf22_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf22.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf22.pdf'>
                    <img class="pdf" data-pdf_no="pdf23" data-link="https://www.cognizant.com/whitepapers/creating-competitive-advantage-from-the-inside-out-codex3666.pdf" src="<?= FRONT_ASSETS ?>img/pdf23_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf23.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf23.pdf'>
                    <img class="pdf" data-pdf_no="pdf24" data-link="https://www.cognizant.com/whitepapers/humans-plus-intelligent-machines-mastering-the-future-of-work-economy-in-asia-pacific-codex3873.pdf" src="<?= FRONT_ASSETS ?>img/pdf24_cover.jpg" data-id='<?= FRONT_ASSETS ?>img/pdf24.png' data-pdf='<?= FRONT_ASSETS ?>img/pdf24.pdf'>
                </div>
            </div>
        </div>
    </div>

    <div class='thepdf'>
        <div class='back'>< BACK</div>
        <img id='loader' src="<?= FRONT_ASSETS ?>img/loader.gif">
        <embed src="<?= FRONT_ASSETS ?>img/pdf1.pdf#toolbar=0" type="application/pdf" width="950px" height="1500px"></embed>
    </div>

    <div class='share_pdf'>
        <div>
            <img src="<?= FRONT_ASSETS ?>img/download.png">
        </div>
        <p>Click the above icon to download as a pdf to your mobile device</p>
    </div>

    <div class='send_pdf'>
        <div>
            <i class="fa fa-close top"></i>
            <i class="fa fa-close bottom"></i>
            <p>To download a copy of this brochure open your camera and focus on the QR code</p>
            <img id='qr' src="<?= FRONT_ASSETS ?>img/qr.png">
            
            <div>
                <p>OR</p>
                <div class='line'></div>
            </div>

            <form id="send_pdf">
                <p>Type in your phone number below</p>
                <input class='number_key' id="phone_input" type="tel" name="phone" placeholder='Phone Number'>
                <input id="link_input" type="hidden" value="">
                <input id="pdf_no" type="hidden" value="">
                <input id='submitPdf' type="submit" value="SEND">
            </form>
        </div>
    </div>

    <div class='success'>
        <div>
            <p>Pdf is on its way!</p>
        </div>
    </div>

    <script type="text/javascript">
        $('.white').fadeOut(1000);
    </script>

    <script type="text/javascript">

        $('.pdf_slider').slick({
            arrows: true,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000
        });


        $('.holder .fa-close').click(function(){
            $('.jQKeyboardContainer').fadeOut(200);
            $.ajax({
                url:'/',
                    type:'GET',
                    success: function(data){
                        var timer;

                        timer = setTimeout(function(){
                            $('.link').css('opacity', '1');
                            $('.content').fadeOut(500);
                            timer = setTimeout(function(){
                                $('.content').html($(data).find('.content').html()).fadeIn();
                                openAnim();
                                $('.clear').css('height', '0');
                                // $('.clear').css('height', '0');
                            }, 500);
                        }, 100);
                    }
            });
        });



        $(document).on('click', '.send_pdf .fa-close', function(e){
            $('.send_pdf').slideUp();
            $('.jQKeyboardContainer').fadeOut(200);
        });

        $(document).on('click', '.fa-close', function(e){
            $('#phone_input').val('');
        });


        $('img.pdf').click(function(e){
            e.preventDefault();
            var pdf_link = $(this).data('link');
            var pdf = $(this).attr('data-pdf');
            var pdf_no = $(this).attr('data-pdf_no');
            $('embed').attr('src', pdf);
            $('#link_input').val(pdf_link);
            $('#pdf_no').val(pdf_no);
        });

        $(document).on('click', '#submitPdf', function(e){
            $('.jQKeyboardContainer').fadeOut(200);
            e.preventDefault();
            var phone = $('#phone_input').val();
            var pdf_link = $('#link_input').val();
            var pdf_no = $('#pdf_no').val();
            $.post("/home/pdfs", {phone: phone, link: pdf_link, pdf_no: pdf_no}, function(response){
                if(response.status){
                    $('.success').slideDown(500);
                    setTimeout(function(){
                        window.location = '/';
                    }, 5000);
                }
            })
            // $('.success').slideDown(500);
            // setTimeout(function(){
            //     window.location = '/';
            // }, 5000);
        });


        $("input[type='tel']").each(function(){
        $(this).on("change input keyup paste", function (e) {
          var output,
            $this = $(this),
            input = $this.val();

          if(e.keyCode != 8) {
            input = input.replace(/[^0-9]/g, '');
            var area = input.substr(0, 3);
            var pre = input.substr(3, 3);
            var tel = input.substr(6, 4);
            // var add = input.substr(10, 4);
            if (area.length < 3) {
              output = "(" + area;
            } else if (area.length == 3 && pre.length < 3) {
              output = "(" + area + ")" + " " + pre;
            } else if (area.length == 3 && pre.length == 3) {
              output = "(" + area + ")" + " " + pre + "-" + tel;
            }
            $this.val(output);
          }
        });
      });


        var keyboard;
            $(function(){
                keyboard = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                            [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                                ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                                ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                                ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                                ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                                ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                                ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                                ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                            ]
                        ]
                    ]
                }
                $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});
                $('textarea.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                var board = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                        ]
                    ]
                }
                $('input.number_key').initKeypad({'donateKeyboardLayout': board});
            });

      (function($){

    var keyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                    ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                    ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                    ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                    ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                    ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                ],
                [
                    ['~', '~', 192, 0, true], ['!', '!', 49, 0, false], ['@', '@', 50, 0, false], ['#', '#', 51, 0, false], ['$', '$', 52, 0, false], ['%', '%', 53, 0, false], ['^', '^', 54, 0, false], 
                    ['&', '&', 55, 0, false], ['*', '*', 56, 0, false], ['(', '(', 57, 0, false], [')', ')', 48, 0, false], ['_', '_', 189, 0, false], ['+', '+', 187, 0, false],
                    ['Q', 'Q', 81, 0, true], ['W', 'W', 87, 0, false], ['E', 'E', 69, 0, false], ['R', 'R', 82, 0, false], ['T', 'T', 84, 0, false], ['Y', 'Y', 89, 0, false], ['U', 'U', 85, 0, false], 
                    ['I', 'I', 73, 0, false], ['O', 'O', 79, 0, false], ['P', 'P', 80, 0, false], ['{', '{', 219, 0, false], ['}', '}', 221, 0, false], ['|', '|', 220, 0, false],
                    ['A', 'A', 65, 0, true], ['S', 'S', 83, 0, false], ['D', 'D', 68, 0, false], ['F', 'F', 70, 0, false], ['G', 'G', 71, 0, false], ['H', 'H', 72, 0, false], ['J', 'J', 74, 0, false], 
                    ['K', 'K', 75, 0, false], ['L', 'L', 76, 0, false], [':', ':', 186, 0, false], ['"', '"', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['Z', 'Z', 90, 0, false], ['X', 'X', 88, 0, false], ['C', 'C', 67, 0, false], ['V', 'V', 86, 0, false], ['B', 'B', 66, 0, false], ['N', 'N', 78, 0, false], 
                    ['M', 'M', 77, 0, false], ['<', '<', 188, 0, false], ['>', '>', 190, 0, false], ['?', '?', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]  
                ]
            ]
        ]
    };


        var donateKeyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['1', '1', 49, 0, true], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                ]
            ]
        ]
    };

    var activeInput = {
        'htmlElem': '',
        'initValue': '',
        'keyboardLayout': keyboardLayout,
        'donateKeyboardLayout': donateKeyboardLayout,
        'keyboardType': '0',
        'keyboardSet': 0,
        'dataType': 'string',
        'isMoney': false,
        'thousandsSep': ',',
        'disableKeyboardKey': false
    };

    /*
     * initialize keyboard
     * @param {type} settings
     */
    $.fn.initKeypad = function(settings){
        //$.extend(activeInput, settings);

        $(this).click(function(e){
            $("#jQKeyboardContainer").remove();
            activateKeypad(e.target);
        });
    };
    
    /*
     * create keyboard container and keyboard button
     * @param {DOM object} targetInput
     */
    function activateKeypad(targetInput){
        if($('div.jQKeyboardContainer').length === 0)
        {
            activeInput.htmlElem = $(targetInput);
            activeInput.initValue = $(targetInput).val();

            $(activeInput.htmlElem).addClass('focus');
            createKeypadContainer();
            createKeypad(0);
        }
    }
    
    /*
     * create keyboard container
     */
    function createKeypadContainer(){
        var container = document.createElement('div');
        container.setAttribute('class', 'jQKeyboardContainer');
        container.setAttribute('id', 'jQKeyboardContainer');
        container.setAttribute('name', 'keyboardContainer' + activeInput.keyboardType);
        
        $('body').append(container);
    }
    
    /*
     * create keyboard
     * @param {Number} set
     */
    function createKeypad(set){
        $('#jQKeyboardContainer').empty();
        if ( $('input.number_key').is(':focus') ) {
            var layout = activeInput.donateKeyboardLayout.layout[activeInput.keyboardType][set];
        }else {
            var layout = activeInput.keyboardLayout.layout[activeInput.keyboardType][set];
        }
        

        for(var i = 0; i < layout.length; i++){

            if(layout[i][4]){
                var row = document.createElement('div');
                row.setAttribute('class', 'jQKeyboardRow');
                row.setAttribute('name', 'jQKeyboardRow');
                $('#jQKeyboardContainer').append(row);
            }

            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('name', 'key' + layout[i][2]);
            button.setAttribute('id', 'key' + layout[i][2]);
            button.setAttribute('class', 'jQKeyboardBtn' + ' ui-button-colspan-' + layout[i][3]);
            button.setAttribute('data-text', layout[i][0]);
            button.setAttribute('data-value', layout[i][1]);
            button.innerHTML = layout[i][0];
            
            $(button).click(function(e){
               getKeyPressedValue(e.target); 
            });

            $(row).append(button);
        }
    }
    /*
     * remove keyboard from kepad container
     */
    function removeKeypad(){
        $('#jQKeyboardContainer').remove();
        $(activeInput.htmlElem).removeClass('focus');
    }
    
    /*
     * handle key pressed
     * @param {DOM object} clickedBtn
     */
    function getKeyPressedValue(clickedBtn){
        var caretPos = getCaretPosition(activeInput.htmlElem);
        var keyCode = parseInt($(clickedBtn).attr('name').replace('key', ''));
        
        var currentValue = $(activeInput.htmlElem).val();
        var newVal = currentValue;
        var closeKeypad = false;
        
        /*
         * TODO
        if(activeInput.isMoney && activeInput.thousandsSep !== ''){
            stripMoney(currentValue, activeInput.thousandsSep);
        }
        */
        
        switch(keyCode){
            case 8:     // backspace key
                newVal = onDeleteKeyPressed(currentValue, caretPos);
              caretPos--;
                break;
            case 13:    // enter key
                closeKeypad = onEnterKeyPressed();
                break;
            case 16:    // shift key
                onShiftKeyPressed();
                break;
            case 27:    // cancel key
                closeKeypad = true;
                newVal = onCancelKeyPressed(activeInput.initValue);
                break;
            case 32:    // space key
                newVal = onSpaceKeyPressed(currentValue, caretPos);
                caretPos++;
    break;
            case 46:    // clear key
                newVal = onClearKeyPressed();
                break;
            case 190:   // dot key
                newVal = onDotKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
            default:    // alpha or numeric key
                newVal = onAlphaNumericKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
        }
        
        // update new value and set caret position
        $(activeInput.htmlElem).val(newVal);
        setCaretPosition(activeInput.htmlElem, caretPos);

        if(closeKeypad){
            removeKeypad();
            $(activeInput.htmlElem).blur();
        }
    }
    
    /*
     * handle delete key pressed
     * @param value 
     * @param inputType
     */
    function onDeleteKeyPressed(value, caretPos){
        var result = value.split('');
        
        if(result.length > 1){
            result.splice((caretPos - 1), 1);
            return result.join('');
        }
    }
    
    /*
     * handle shift key pressed
     * update keyboard layout and shift key color according to current keyboard set
     */
    function onShiftKeyPressed(){
        var keyboardSet = activeInput.keyboardSet === 0 ? 1 : 0;
        activeInput.keyboardSet = keyboardSet;

        createKeypad(keyboardSet);
        
        if(keyboardSet === 1){
            $('button[name="key16"').addClass('shift-active');
        }else{
            $('button[name="key16"').removeClass('shift-active');
        }
    }
    
    /*
     * handle space key pressed
     * add a space to current value
     * @param {String} curVal
     * @returns {String}
     */
    function onSpaceKeyPressed(currentValue, caretPos){
        return insertValueToString(currentValue, ' ', caretPos);
    }
    
    /*
     * handle cancel key pressed
     * revert to original value and close key pad
     * @param {String} initValue
     * @returns {String}
     */
    function onCancelKeyPressed(initValue){
        return initValue;
    }
    
    /*
     * handle enter key pressed value
     * TODO: need to check min max value
     * @returns {Boolean}
     */
    function onEnterKeyPressed(){
        return true;
    }
    
    /*
     * handle clear key pressed
     * clear text field value
     * @returns {String}
     */
    function onClearKeyPressed(){
        return '';
    }
    
    /*
     * handle dot key pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onDotKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * handle all alpha numeric keys pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onAlphaNumericKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * insert new value to a string at specified position
     * @param {String} currentValue
     * @param {String} newValue
     * @param {Number} pos
     * @returns {String}
     */
    function insertValueToString(currentValue, newValue, pos){
        var result = currentValue.split('');
        result.splice(pos, 0, newValue);
        
        return result.join('');
    }
    
   /*
    * get caret position
    * @param {DOM object} elem
    * @return {Number}
    */
    function getCaretPosition(elem){
        var input = $(elem).get(0);

        if('selectionStart' in input){    // Standard-compliant browsers
            return input.selectionStart;
        } else if(document.selection){    // IE
            input.focus();
            
            var sel = document.selection.createRange();
            var selLen = document.selection.createRange().text.length;
            
            sel.moveStart('character', -input.value.length);
            return sel.text.length - selLen;
        }
    }
    
    /*
     * set caret position
     * @param {DOM object} elem
     * @param {Number} pos
     */
    function setCaretPosition(elem, pos){
        var input = $(elem).get(0);
        
        if(input !== null) {
            if(input.createTextRange){
                var range = elem.createTextRange();
                range.move('character', pos);
                range.select();
            }else{
                input.focus();
                input.setSelectionRange(pos, pos);
            }
        }
    }
})(jQuery);






    </script>
    <style type="text/css">
          #key8 {
            margin-top: 19px;
          }
    </style>
</div>