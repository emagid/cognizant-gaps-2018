<section class='homepage quiz_template' style='background-image: url(<?= FRONT_ASSETS ?>img/background.jpg);'>
<div class='content'>
  <div class='holder'>  
    <i class="fa fa-close top"></i>
    <i class="fa fa-close bottom"></i>  
    <section class='inner_page page quiz_page'>
        <div class='links'>
          <p class='title'>The Thought Leadership<br> Rating Game</p>
          <p>Are you a thought leadership expert? Want to help us improve the quality of our thought leadership?</p>
  <!--         <p>Read the following short-form thought leadership assets using the following attributes and grade themon a 0-5 scale where 0 is mehand 5 is excellent.</p> -->
          <p>Choose one of the following short-form thought leadership assets, and then grade it! </p>
          <div class='question'>
            <div id='1' class='answer'>
              <p>Hybrid Cloud</p>
            </div>
            <div id='2' class='answer'>
              <p>Bending the IT Op-Ex Cost </p>
            </div>
            <div id='3' class='answer'>
              <p>There Will Be Jobs in the Future of Work</p>
            </div>

            <p><span class='green'>Each article is a 5-minute read. The more you read, the better your chances of winning a $25 Amazon Gift Card.</span></p>
          </div>
        </div>
    </section>
  </div>

  <div class='grade'>
    <i class="fa fa-close top"></i>
    <i class="fa fa-close bottom"></i>  
    <div class='grade_holder'>
      <p class='title'>Rate the article to win!</p>
      <p>Adjust the slider below, and rate the article from 1 to 100. Then enter your email and press submit.</p>
      <p>While rating the article keep in mind these attributes: <strong>Focus</strong>, <strong>Novelty</strong>, <strong>Relevance</strong>, <strong>Validity</strong>, <strong>Rigor</strong>, <strong>Clarity</strong></p>
      <div class="slidecontainer">
        <input type="range" min="1" max="100" value="50" class="slider" id="myRange">
      </div>
      <p class='score'><span>50</span> / 100</p>

      <form id='the_grade'>
        <input id="article" type="hidden" name="article" value="">
        <input id='score' type="hidden" name="score">
        <input id="email" class='jQKeyboard' type="text" name="email" placeholder='Enter your email'>
        <input class='quiz_submit' type="submit" value="ENTER EMAIL TO SUBMIT">
        <p class='error'>Please enter an email</p>
      </form>
    </div>
  </div>

  <div class='quiz_complete'>
    <div>
      <p>Thank you!</p>
      <p>Please email <strong>cognizantTL@cognizant.com</strong> with thought leadership improvement suggestions. Or to find out the average score for your article.</p>
      <button class='rate_again'>RATE ANOTHER</button>
      <p>Increase your chance to win!</p>
    </div>
  </div>

  <section class='read'>
    <div class='back'>< BACK</div>
    <div class='take_quiz'>GRADE ARTICLE</div>

    <!-- ARTICLE ONE -->
    <div id='article1' class='article'>
      <h1>HYBRID CLOUD: A KEY COMPONENT FOR DOING BUSINESS DIGITALLY</h1>
      <div class="txt">
        <p>Not too long ago, the main focus for businesses was efficiency and stability, and IT operations were measured in terms of cost and service availability. With the influx of digital technologies and mindsets, and the ensuing changes to long-held business paradigms, the new focus for IT is agility: the ability to respond quickly to market changes, accelerate innovation to offer personalized services and counter competitors.</p>
        <p>While many digital natives can use a straightforward cloud approach to enable agility and innovation, industry stalwarts need to consider what they’ve already got in place. The answer can’t be just private or public cloud but, instead, orchestrating and managing workloads across multiple technologies and platforms in a hybrid cloud environment, which incorporates elements of both.</p>
        <p>Hybrid cloud allows enterprises to dynamically orchestrate applications between the public and private cloud, and unify the architecture under integrated processes, tools and governance. Indeed, hybrid cloud has become an integral element of successful digital pursuits for traditional businesses. Gartner predicts that by 2020, 90% of organizations will adopt hybrid infrastructure management capabilities. MarketsandMarkets projects a near tripling of the hybrid cloud market from $33 billion in 2016 to $92 billion by 2021. The primary reasons cited for adopting hybrid cloud solutions: operational efficiencies, a right-fit landing platform for workloads, agility and commercial flexibility.</p>
        
      </div>

      <div class="txt txt_white">
          <p>A hybrid cloud solution is the right way forward for several very practical business scenarios: </p>
          <ul>
            <li><span><strong>Workload portability and flexibility:</strong> Setting up a dev/test, lab and training environment on a public cloud platform and cross-leveraging an on-premise environment for production workloads.</span></li>
            <li><span><strong>Collaboration across workloads:</strong> Seamlessly integrating existing applications and business processes with public cloud services.</span></li>
            <li><span><strong>Business expansion: </strong> Setting up a disaster recovery, data backup and business continuity strategy when expanding into new geographies.</span></li>
            <li><span><strong>Consolidation and migration:</strong> Implementing IT consolidation and migration projects.</span></li>
          </ul>
      </div> 

      <div class='txt'>
        <h3>Overcoming Business Challenges with Hybrid Cloud</h3>
        <p>Hybrid cloud can enable businesses to overcome key challenges:</p>
        <ul>
            <li><span><strong>Leverage existing skillsets, tools and investments. </strong> Businesses can optimize existing IT assets and skillsets by integrating, extending and centrally managing the on-premise and cloud environments.</span></li>
            <li><span><strong>Minimize disruption </strong> with gradual and efficient change management of the technology platform, service operations and inventory management.</span></li>
            <li><span><strong>Achieve consistent operational and service levels. </strong> Using a single monitoring and management console, businesses can enable unified service levels for enterprise applications and service availability.</span></li>
            <li><span><strong>Secure customer data. </strong> Comprehensive security and governance can be provided at each layer of service operation, infrastructure and applications. </span></li>
          </ul>
      </div>

      <div class="txt txt_white">
        <h3>Enabling Right-fit Solutions</h3>
        <p>Because the hybrid cloud solution needs to be aligned with the enterprise’s specific business needs, it’s important to utilize standardized service delivery frameworks and pre-packaged service catalogs, which can be tailored and extended to meet changing business requirements.  These requirements can include the need to increase operational agility, flexibility or scalability by implementing proper landing platforms, which can also reduce the overall cost of service operation management.</p>
        <p>The various forms of value realized through a hybrid cloud solution include:</p>
        <ul>
            <li><span><strong>Enhanced customer experience </strong> through an integrated platform and collaborative and self-service capabilities.</span></li>
            <li><span><strong>Accelerated innovation </strong> through integration of technology silos, comprehensive governance across shadow IT groups, continuous development and smart service operations (powered by artificial intelligence and machine learning).</span></li>
            <li><span><strong>Increased agility, </strong> through application portability and leveraging of microservices, containers and DevOps.</span></li>
            <li><span><strong>Scalability, </strong> with the option to start small and seamlessly scale along with business needs, significantly reducing overall capital expenditures.</span></li>
          </ul>
      </div>

      <div class="txt">
        <h3>Enabling Right-fit Solutions</h3>
        <p>We worked with a leading financial institution to navigate its transition to digital by assessing its workloads, consolidating its data center and implementing a consumption-based, software-defined hybrid cloud environment with tiered cloud landing zones.</p>
        <p>By deploying a consumption-based model, the global payments organization was able to reduce costs, improve efficiencies and embrace a next-gen operating model. The shift to digital also enabled the company to maintain its leadership position by allowing it to address the diverse needs of both digitally savvy millennial customers and traditional clients, all through one platform based on hybrid cloud.</p>
        <p>To meet their goals of digital transformation, businesses need to shift from a departmentally based approach, to a broader, enterprise-wide strategy. With the flexibility of hybrid cloud, and a successful roadmap, legacy businesses can prepare themselves for the digital future.</p>
      </div>
    </div>


      <!-- ARTICLE 2 -->
      <div id='article2' class='article'>
        <h1>Bending the IT Op-Ex Cost Curve Through IT Simplification</h1>
        <div class="txt">
          <p>Today’s CIOs grapple with a multitude of inherited challenges. From an inside-out IT portfolio perspective, these obstacles include:</p>
          <ul>
              <li><span>A massive and unstructured expanse of IT applications.</span></li>
              <li><span>Heterogeneous and expensive technology architectures.</span></li>
              <li><span>Legacy and duplicate infrastructure.</span></li>
              <li><span>Inefficient processes and skills shortages.</span></li>
              <li><span>Overly complex workflows.</span></li>
            </ul>
            <p>As such, IT complexity is a multidimensional problem that cannot be solved by selectively addressing discrete elements. Architecture and systems management tools impact infrastructure and application choices. Legacy modernization investments should be considered alongside new digital capabilities and partnering decisions. Automation should be applied synergistically across both applications and infrastructure. And processes, operating models and governance work best when they are interlocking and facilitate an agile, empowered and innovative work environment.</p>
            <p>Achieving long-lasting efficiency and value benefits requires the orchestration of diverse elements of the IT environment to create a set of cohesive, well-managed services and business-oriented outcomes.</p>
        </div>

        <div class="txt txt_white">
            <h3>A Proven IT Simplification Framework</h3>
            <img src="<?= FRONT_ASSETS ?>img/fig1.png">
            <p>CIOs can simplify and refine their enterprise IT landscape through a structured, holistic and well-governed strategy that can successfully bend the operating expense (Op-Ex) curve and pave the way for a systematic digital journey (see above image). We find the following four-step approach to be an excellent starting point. It focuses sequentially on enterprise IT inventory and strategy; process, tools and operating model optimization; application infrastructure optimization and cloud migration; and IT operations automation.</p>
        </div> 

        <div class='txt'>
          <p><strong>Enterprise IT inventory and strategy:</strong> Bending the Op-Ex cost curve begins with taking a hard look at what your organization currently spends on common architecture and tools across business entities — not only capturing direct payments to external vendors, but also overhead costs such as internal staff hours and effort. Start with these key questions:</p>
          <ul>
              <li><span>Which systems are under- and over-utilized; which have the highest outage rates?</span></li>
              <li><span>Which require significant work-arounds to accommodate new technology and re-provisioning?</span></li>
              <li><span>Which receive the highest number of user and admin complaints?</span></li>
          </ul>

          <p><strong>Process, tools and operating model optimization:</strong> During this phase, your team will review the process maturity of your plan-build-run organization compared with industry standards and best practices such as IT4IT, PMBOK, CMMI and ITIL. Adopting the DevOps discipline of continuous integration/continuous delivery (CI/CD) with an integrated platform strategy can significantly reduce both effort and time-to-market.</p>
          <p><strong>IT operations automation:</strong> Addressing trouble tickets and backlogs can provide a unique opportunity for understanding IT issues, as well as identifying low-hanging fruit for cost reduction through automation. Our approach to trouble tickets is to categorize them as technical, operational, functional or knowledge debt, and as either avoidable or unavoidable.</p>
        </div>

        <div class='txt txt_white'>
          <h3>Application Portfolio Rationalization & Cloud Readiness</h3>
          <p>Organizations can’t remain competitive or relevant without rationalizing their complex, inefficient and expensive IT systems into a simpler, high-performing and cost-efficient IT portfolio. To achieve this, the IT portfolio must be aligned with the organization’s business objectives and must be flexible enough to address ever-changing business demands. This means that legacy applications, traditional operating models and low performing assets and processes must be quickly assessed for future fit.</p>
          <p>To begin, companies should complete an objective, deep-dive analysis of the business and technical costs and contribution of each application in the IT portfolio. Based on the relative value each application provides, the organization can decide to retire, retain, replace or refactor.</p>
          <p>Once a portfolio is constructed that can deliver/act faster and provide higher business throughput, organizations need to think of cloud-based solutions to unlock the potential of digital business opportunities. By performing a cloud-readiness assessment, your organization will be able to unearth the potential cost advantages compared with owning and managing all applications portfolios on premises. In addition, it can access predictive analytics, auto-scaling options, add-ons that support certain business requirements, and integration of technologies for better controls and monitoring.</p>
        </div>

        <div class='txt'>
          <h3>Tangible, Real-Life Benefits</h3>
          <p>Implementing an IT simplification framework can provide dramatic, long-term savings. A major U.S. healthcare company whose Op-Ex had been growing by almost 13% year-over-year recently applied this approach to identify $48.8 million of annual savings on a $232 million base — a 21% annual reduction in Op-Ex.</p>

          <h3>Looking Forward</h3>
          <p>While business leaders recognize the need to leverage digital technologies to transform their business and keep pace with rivals, they also must balance this pursuit with other critical investment priorities, such as R&D, sales and marketing, and customer support. The primary benefit of completing a global IT simplification framework is that Op-Ex savings can be substantial, and typically, any savings achieved in operating efficiencies can be retained within the IT organization and applied to strategic projects.</p>
          <p>To achieve substantial Op-Ex savings, evaluate your IT environment holistically, anticipate touchpoints and dependencies, and identify and prioritize opportunities to simplify, modernize and secure your organizations’ digital backbone. Create a proven framework for identifying and remediating portfolio inefficiencies. </p>
          <p>A best-practices framework can help your organization to efficiently and effectively do the following:</p>
          <ul>
              <li><span>Inventory your “as is” IT environment and develop your desired “to be” enterprise IT strategy and roadmap.</span></li>
              <li><span>Identify opportunities to reduce licensing fees, and move IT processes from task-based to managed services or outcome-based models.</span></li>
              <li><span>Analyze the trouble-ticket repository and apply debt analytics to identify opportunities for effort elimination and work automation.</span></li>
              <li><span>Analyze application portfolio utilization, cost and contribution, and identify candidates to retain, replace, refactor or retire. Look for opportunities to leverage cloud-based technologies to reduce costs and increase operational flexibility and scalability.</span></li>
              <li><span>Adopt a continuous development/integration mindset leveraging Agile and DevOps to accelerate time-to-market and reduce costs and rework.</span></li>
          </ul>
        </div>

        <div class='txt txt_white'>
          <p>While completing a holistic analysis and roadmap of your Op-Ex environment does involve an upfront investment, either by dedicating internal resources or hiring outside expertise, the investment will return results year after year. It will free up funds and resources your team can dedicate to much higher-value strategic and transformational opportunities.</p>
          <p>To learn more, read our white paper “Bending the IT Op-Ex Cost Curve Through IT Simplification,” visit the Digital Systems & Technology section of our website, or contact us.</p>
        </div>
      </div>

      <!-- ARTICLE3 -->
      <div id='article3' class='article'>
        <h1>There Will Be Jobs in the Future of Work</h1>
        <div class="txt">
          <p>Just a year ago, we published our report “21 Jobs of the Future: A Guide to Getting and Staying Employed in the Next 10 Years.” It clearly hit a nerve: Articles and interviews ran in media across the world, and the report was the talk of many important towns during the year, including the World Economic Forum in Davos, the Atlantic Festival in Washington D.C., the Aspen Action Forum in Colorado, and the Bilderberg Meeting in Turin, Italy. In a landscape dominated by coverage of an AI/robotics-driven jobs apocalypse, our countervailing point of view, based on deep analysis rather than simply wishful thinking, provided some balance to the ongoing debate of “whither jobs?"</p>
          <p>We’ve been excited to see that many of the jobs we postulated in last year’s report have actually started to become reality before our very eyes, including our Financial Wellness Coach (appearing as Metlife’s PlanSmart Financial Wellness), our Man Machine Teaming Manager (cropping up as a Robot Manager at Cobalt Robotics) and our Memory Curator and Augmented Reality Journey Builder jobs (brought to life at Facebook).</p>
          <p>That’s why, just one year later, we’ve come up with 21 more jobs that we believe will emerge in the next decade. As we did last year, we identified three themes for this year’s jobs that underscore our belief that no matter how technological our age becomes, we, as humans, want the human touch. The jobs we propose all serve the very human desire for:</p>
          <ul>
              <li><span><strong>Ethical behaviors: </strong> We want machines — and humankind — to act well and behave themselves.</span></li>
              <li><span><strong>Security and safety: </strong> We want to feel safe in this brave (and scary) new world we’re creating.</span></li>
              <li><span><strong>Dreams: </strong> We want to continue believing in the positive power of technological-driven prog­ress.</span></li>
          </ul>
          <p>In other words, all of the jobs we list require important work that humans will continue to need to do.</p>
          
        </div>

        <div class="txt txt_white">
            <h3>Unveiling Your Future</h3>
            <p>Our interactive graphic illustrates 21 jobs we believe will emerge between now and 2029, see Figure 1. As we did last year, we’ve positioned them over a 10-year timeline and according to their “tech-centricity.” Some are highly technical, while others won’t require much tech knowledge at all.</p>
            <img src="<?= FRONT_ASSETS ?>img/fig2.png">
        </div> 

        <div class='txt'>
          <h3>One’s End Is Another’s Beginning</h3>
          <p>In sum, we’ve now presented 42 jobs of the future, see Figure 2. In conjunction with this year’s report, we’ve also created the Cognizant Jobs of the Future (CJoF) Index that on a quarterly basis will quantify the scale and velocity of emerging new types of work. More details about the CJoF Index can be found at https://cogniz.at/jobsindex.</p>
          <img src="<?= FRONT_ASSETS ?>img/fig3.jpg">
        </div>

        <div class='txt txt_white'>
          <p>The fear, uncertainty and doubt (and disgust) that is abroad at the moment is the product of a world changing at an unprecedented speed. This is natural, this is understandable, but most importantly — particularly if you’re a senior leader — this is a mistake. The world has always changed, and always will continue to change. The jobs we do have always changed, and always will continue to change.</p>
          <p>We said last year that “you never know, one day, you might be doing one of them” — and for some people that has turned out to be true. Our jobs of the future may be against the natural order of things, but they are the future. Engage …</p>
          <p>For the entire report and a full description of all 21 jobs, see “21 More Jobs of the Future: A Guide to Getting — and Staying — Employed through 2019,” or learn more at our “21 More Jobs of the Future: Humans Needed!” webinar.” Visit us at Cognizant’s Center for the Future of Work.</p>
        </div>

    </section>



      <script>
          $(document).on('click','.fa-close', function(){
            $('.jQKeyboardContainer').fadeOut(400);
              $.ajax({
                  url:'/',
                      type:'GET',
                      success: function(data){
                          var timer;

                          timer = setTimeout(function(){
                              $('.link').css('opacity', '1');
                              $('.content').fadeOut(500);
                              timer = setTimeout(function(){
                                  $('.content').html($(data).find('.content').html()).fadeIn();
                                  openAnim();
                                  $('.clear').css('height', '0');
                                  // $('.clear').css('height', '0');
                              }, 500);
                          }, 100);
                      }
              });
          });

          $(document).on('click', '.back', function(){
            $('.read').fadeOut(1000);
            $('.read').css('height', '0');
            $('.read .article').fadeOut(1000);
          });

          $(document).on('click', '.take_quiz', function(){
            $('.grade').fadeIn(1000);
            $('.grade').css('height', '100%');

            setTimeout(function(){
              $('.read').fadeOut(1000);
              $('.read').css('height', '0');
            }, 1000);
          });

          $(document).on('click', '.answer', function(){
            var self = $(this);
            var id = "#article" + $(this).attr('id');
            $("#article").val($(this).attr('id'));

            $(self).css('transform', 'scale(.95)');
            $('.read').fadeIn(1000);
            $('.read').css('height', '100%');
            $(id).fadeIn(500);

            timer = setTimeout(function(){
                $(self).css('transform', 'scale(1)');
            }, 200);
          });


         $(document).on('change', '#myRange', function(){
          $('.score span').html($('#myRange').val());
         });


         $(document).on('click', '.jQKeyboard', function(){
          $(this).css('margin-top', '299px');
         });


         $(document).on('click', '.quiz_submit', function(e){
          e.preventDefault();
          if ( $('.jQKeyboard').val() == "" ) {
            $('.error').slideDown();
          }else {
            var score = $('#myRange').val();
            $('#score').attr('value', score);
            var article = $("#article").val();
            var email = $("#email").val();
            $.post("/home/submit_quiz",{email: email, rating: score, article: article}, function(response){
              console.log(response);
              $('.quiz_complete').slideDown();
              $('.jQKeyboardContainer').hide();
              var timer;
              timer = setTimeout(function(){
                $('#the_grade').submit();
                window.location = '/';
              }, 10000);

              $('.rate_again').click(function(){
                clearTimeout(timer);
                $('#the_grade').submit();
                window.location = '/home/quiz';
              });
            });
          }
         });




         // KEYBOARD
         var keyboard;
              $(function(){
                  keyboard = {
                      'layout': [
                          // alphanumeric keyboard type
                          // text displayed on keyboard button, keyboard value, keycode, column span, new row
                          [
                              [
                                  ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                  ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                                  ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                                  ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                                  ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                                  ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                                  ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                                  ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                                  ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                              ]
                          ]
                      ]
                  }
                  $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});
                  $('textarea.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                  var board = {
                      'layout': [
                          // alphanumeric keyboard type
                          // text displayed on keyboard button, keyboard value, keycode, column span, new row
                          [
                                  ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                  ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                          ]
                      ]
                  }
                  $('input.number_key').initKeypad({'donateKeyboardLayout': board});
              });

        (function($){

      var keyboardLayout = {
          'layout': [
              // alphanumeric keyboard type
              // text displayed on keyboard button, keyboard value, keycode, column span, new row
              [
                  [
                      ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                      ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                      ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                      ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                      ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                      ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                      ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                      ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                      ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                  ],
                  [
                      ['~', '~', 192, 0, true], ['!', '!', 49, 0, false], ['@', '@', 50, 0, false], ['#', '#', 51, 0, false], ['$', '$', 52, 0, false], ['%', '%', 53, 0, false], ['^', '^', 54, 0, false], 
                      ['&', '&', 55, 0, false], ['*', '*', 56, 0, false], ['(', '(', 57, 0, false], [')', ')', 48, 0, false], ['_', '_', 189, 0, false], ['+', '+', 187, 0, false],
                      ['Q', 'Q', 81, 0, true], ['W', 'W', 87, 0, false], ['E', 'E', 69, 0, false], ['R', 'R', 82, 0, false], ['T', 'T', 84, 0, false], ['Y', 'Y', 89, 0, false], ['U', 'U', 85, 0, false], 
                      ['I', 'I', 73, 0, false], ['O', 'O', 79, 0, false], ['P', 'P', 80, 0, false], ['{', '{', 219, 0, false], ['}', '}', 221, 0, false], ['|', '|', 220, 0, false],
                      ['A', 'A', 65, 0, true], ['S', 'S', 83, 0, false], ['D', 'D', 68, 0, false], ['F', 'F', 70, 0, false], ['G', 'G', 71, 0, false], ['H', 'H', 72, 0, false], ['J', 'J', 74, 0, false], 
                      ['K', 'K', 75, 0, false], ['L', 'L', 76, 0, false], [':', ':', 186, 0, false], ['"', '"', 222, 0, false], ['Enter', '13', 13, 3, false],
                      ['Shift', '16', 16, 2, true], ['Z', 'Z', 90, 0, false], ['X', 'X', 88, 0, false], ['C', 'C', 67, 0, false], ['V', 'V', 86, 0, false], ['B', 'B', 66, 0, false], ['N', 'N', 78, 0, false], 
                      ['M', 'M', 77, 0, false], ['<', '<', 188, 0, false], ['>', '>', 190, 0, false], ['?', '?', 191, 0, false], ['Shift', '16', 16, 2, false],
                      ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]  
                  ]
              ]
          ]
      };


          var donateKeyboardLayout = {
          'layout': [
              // alphanumeric keyboard type
              // text displayed on keyboard button, keyboard value, keycode, column span, new row
              [
                  [
                      ['1', '1', 49, 0, true], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                      ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                  ]
              ]
          ]
      };

      var activeInput = {
          'htmlElem': '',
          'initValue': '',
          'keyboardLayout': keyboardLayout,
          'donateKeyboardLayout': donateKeyboardLayout,
          'keyboardType': '0',
          'keyboardSet': 0,
          'dataType': 'string',
          'isMoney': false,
          'thousandsSep': ',',
          'disableKeyboardKey': false
      };

      /*
       * initialize keyboard
       * @param {type} settings
       */
      $.fn.initKeypad = function(settings){
          //$.extend(activeInput, settings);

          $(this).click(function(e){
              $("#jQKeyboardContainer").remove();
              activateKeypad(e.target);
          });
      };
      
      /*
       * create keyboard container and keyboard button
       * @param {DOM object} targetInput
       */
      function activateKeypad(targetInput){
          if($('div.jQKeyboardContainer').length === 0)
          {
              activeInput.htmlElem = $(targetInput);
              activeInput.initValue = $(targetInput).val();

              $(activeInput.htmlElem).addClass('focus');
              createKeypadContainer();
              createKeypad(0);
          }
      }
      
      /*
       * create keyboard container
       */
      function createKeypadContainer(){
          var container = document.createElement('div');
          container.setAttribute('class', 'jQKeyboardContainer');
          container.setAttribute('id', 'jQKeyboardContainer');
          container.setAttribute('name', 'keyboardContainer' + activeInput.keyboardType);
          
          $('body').append(container);
      }
      
      /*
       * create keyboard
       * @param {Number} set
       */
      function createKeypad(set){
          $('#jQKeyboardContainer').empty();
          if ( $('input.number_key').is(':focus') ) {
              var layout = activeInput.donateKeyboardLayout.layout[activeInput.keyboardType][set];
          }else {
              var layout = activeInput.keyboardLayout.layout[activeInput.keyboardType][set];
          }
          

          for(var i = 0; i < layout.length; i++){

              if(layout[i][4]){
                  var row = document.createElement('div');
                  row.setAttribute('class', 'jQKeyboardRow');
                  row.setAttribute('name', 'jQKeyboardRow');
                  $('#jQKeyboardContainer').append(row);
              }

              var button = document.createElement('button');
              button.setAttribute('type', 'button');
              button.setAttribute('name', 'key' + layout[i][2]);
              button.setAttribute('id', 'key' + layout[i][2]);
              button.setAttribute('class', 'jQKeyboardBtn' + ' ui-button-colspan-' + layout[i][3]);
              button.setAttribute('data-text', layout[i][0]);
              button.setAttribute('data-value', layout[i][1]);
              button.innerHTML = layout[i][0];
              
              $(button).click(function(e){
                 getKeyPressedValue(e.target); 
              });

              $(row).append(button);
          }
      }
      /*
       * remove keyboard from kepad container
       */
      function removeKeypad(){
          $('#jQKeyboardContainer').remove();
          $(activeInput.htmlElem).removeClass('focus');
      }
      
      /*
       * handle key pressed
       * @param {DOM object} clickedBtn
       */
      function getKeyPressedValue(clickedBtn){
          var caretPos = getCaretPosition(activeInput.htmlElem);
          var keyCode = parseInt($(clickedBtn).attr('name').replace('key', ''));
          
          var currentValue = $(activeInput.htmlElem).val();
          var newVal = currentValue;
          var closeKeypad = false;
          
          /*
           * TODO
          if(activeInput.isMoney && activeInput.thousandsSep !== ''){
              stripMoney(currentValue, activeInput.thousandsSep);
          }
          */
          
          switch(keyCode){
              case 8:     // backspace key
                  newVal = onDeleteKeyPressed(currentValue, caretPos);
                caretPos--;
                  break;
              case 13:    // enter key
                  closeKeypad = onEnterKeyPressed();
                  break;
              case 16:    // shift key
                  onShiftKeyPressed();
                  break;
              case 27:    // cancel key
                  closeKeypad = true;
                  newVal = onCancelKeyPressed(activeInput.initValue);
                  break;
              case 32:    // space key
                  newVal = onSpaceKeyPressed(currentValue, caretPos);
                  caretPos++;
      break;
              case 46:    // clear key
                  newVal = onClearKeyPressed();
                  break;
              case 190:   // dot key
                  newVal = onDotKeyPressed(currentValue, $(clickedBtn), caretPos);
                  caretPos++;
                  break;
              default:    // alpha or numeric key
                  newVal = onAlphaNumericKeyPressed(currentValue, $(clickedBtn), caretPos);
                  caretPos++;
                  break;
          }
          
          // update new value and set caret position
          $(activeInput.htmlElem).val(newVal);
          setCaretPosition(activeInput.htmlElem, caretPos);

          if(closeKeypad){
              removeKeypad();
              $(activeInput.htmlElem).blur();
          }
      }
      
      /*
       * handle delete key pressed
       * @param value 
       * @param inputType
       */
      function onDeleteKeyPressed(value, caretPos){
          var result = value.split('');
          
          if(result.length > 1){
              result.splice((caretPos - 1), 1);
              return result.join('');
          }
      }
      
      /*
       * handle shift key pressed
       * update keyboard layout and shift key color according to current keyboard set
       */
      function onShiftKeyPressed(){
          var keyboardSet = activeInput.keyboardSet === 0 ? 1 : 0;
          activeInput.keyboardSet = keyboardSet;

          createKeypad(keyboardSet);
          
          if(keyboardSet === 1){
              $('button[name="key16"').addClass('shift-active');
          }else{
              $('button[name="key16"').removeClass('shift-active');
          }
      }
      
      /*
       * handle space key pressed
       * add a space to current value
       * @param {String} curVal
       * @returns {String}
       */
      function onSpaceKeyPressed(currentValue, caretPos){
          return insertValueToString(currentValue, ' ', caretPos);
      }
      
      /*
       * handle cancel key pressed
       * revert to original value and close key pad
       * @param {String} initValue
       * @returns {String}
       */
      function onCancelKeyPressed(initValue){
          return initValue;
      }
      
      /*
       * handle enter key pressed value
       * TODO: need to check min max value
       * @returns {Boolean}
       */
      function onEnterKeyPressed(){
          return true;
      }
      
      /*
       * handle clear key pressed
       * clear text field value
       * @returns {String}
       */
      function onClearKeyPressed(){
          return '';
      }
      
      /*
       * handle dot key pressed
       * @param {String} currentVal
       * @param {DOM object} keyObj
       * @returns {String}
       */
      function onDotKeyPressed(currentValue, keyElement, caretPos){
          return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
      }
      
      /*
       * handle all alpha numeric keys pressed
       * @param {String} currentVal
       * @param {DOM object} keyObj
       * @returns {String}
       */
      function onAlphaNumericKeyPressed(currentValue, keyElement, caretPos){
          return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
      }
      
      /*
       * insert new value to a string at specified position
       * @param {String} currentValue
       * @param {String} newValue
       * @param {Number} pos
       * @returns {String}
       */
      function insertValueToString(currentValue, newValue, pos){
          var result = currentValue.split('');
          result.splice(pos, 0, newValue);
          
          return result.join('');
      }
      
     /*
      * get caret position
      * @param {DOM object} elem
      * @return {Number}
      */
      function getCaretPosition(elem){
          var input = $(elem).get(0);

          if('selectionStart' in input){    // Standard-compliant browsers
              return input.selectionStart;
          } else if(document.selection){    // IE
              input.focus();
              
              var sel = document.selection.createRange();
              var selLen = document.selection.createRange().text.length;
              
              sel.moveStart('character', -input.value.length);
              return sel.text.length - selLen;
          }
      }
      
      /*
       * set caret position
       * @param {DOM object} elem
       * @param {Number} pos
       */
      function setCaretPosition(elem, pos){
          var input = $(elem).get(0);
          
          if(input !== null) {
              if(input.createTextRange){
                  var range = elem.createTextRange();
                  range.move('character', pos);
                  range.select();
              }else{
                  input.focus();
                  input.setSelectionRange(pos, pos);
              }
          }
      }
  })(jQuery);
      </script>


  <style type="text/css">
    div.jQKeyboardContainer {
      top: 901px;
    }

    #key8 {
      margin-top: 19px;
    }
  </style>
  </div>
</section>

